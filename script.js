let tabs = document.querySelector(".centered-content");
let tabsTitle = document.querySelectorAll(".tabs-title");
let contents = document.querySelectorAll(".content");

tabs.addEventListener("click", changeTabs);

function changeTabs (e) {
    let id = e.target.dataset.id;
  if (id) {
    tabsTitle.forEach(tabs => {
      tabs.classList.remove("active");
    });
    e.target.classList.add("active");
       
    contents.forEach(content => {
        content.classList.remove("contentActive");
    });
    let element = document.getElementById(id);
    element.classList.add("contentActive");
  }
}
